package com.company;

import com.company.exceptions.InvalidAmountException;
import com.company.exceptions.InvalidArgumentException;
import com.company.exceptions.NoChangeException;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Nicolas Pereira on 6/10/16.
 *
 */
public class CashMachine {
    private List<MoneySlot> moneySlots;

    public CashMachine() {
        moneySlots = new ArrayList<MoneySlot>();
        moneySlots.add(new MoneySlot(20));
        moneySlots.add(new MoneySlot(10));
        moneySlots.add(new MoneySlot(5));
        moneySlots.add(new MoneySlot(2));
        moneySlots.add(new MoneySlot(1));
    }

    public List<MoneySlot> getMoneySlots() {
        return moneySlots;
    }

    public int getTotal() {
        int total = 0;
        for (int i = 0; i < moneySlots.size(); i++) {
            total = total + moneySlots.get(i).getTotalInSLot();
        }
        return total;
    }

    public String show() {
        StringBuilder sb = new StringBuilder();

        sb.append("$");
        sb.append(getTotal());
        sb.append(" ");

        for (int i = 0; i < moneySlots.size(); i++) {
            sb.append(moneySlots.get(i).getQuantity() + " ");
        }

        return sb.toString().trim();
    }

    public String put(int... args) throws InvalidArgumentException {
        if (args.length != moneySlots.size()) throw new InvalidArgumentException();

        for (int i = 0; i < args.length; i++) {
            moneySlots.get(i).add(args[i]);
        }
        return show();
    }

    public String take(int... args) throws InvalidArgumentException, InvalidAmountException {
        if (args.length != moneySlots.size()) throw new InvalidArgumentException();
        if (!validateAmounts(args)) throw new InvalidAmountException();

        for (int i = 0; i < args.length; i++) {
            moneySlots.get(i).remove(args[i]);
        }
        return show();
    }

    protected boolean validateAmounts(int[] args) {
        boolean isValid = true;
        for (int i = 0; i < args.length; i++) {
            if (!moneySlots.get(i).isValidRemove(args[i])) {
                isValid = false;
                break;
            }
        }
        return isValid;
    }

    public String change(int moneyToChange) throws InvalidAmountException, NoChangeException {

        if (moneyToChange > getTotal()) throw new InvalidAmountException();

        return calculateChange(moneyToChange);
    }

    private String calculateChange(int moneyToChange) throws NoChangeException {

        CashMachine cashback = new CashMachine();

        int totalToRemove = moneyToChange;

        for (int i = 0; i < moneySlots.size(); i++) {
            totalToRemove = removeFromSlot(moneySlots.get(i), totalToRemove, cashback.getMoneySlots().get(i));
        }

        if (totalToRemove != 0) {
            this.add(cashback);
            throw new NoChangeException();
        }
        return cashback.show();
    }

    private void add(CashMachine cashMachineToRemove) {
        for (int i = 0; i < moneySlots.size(); i++) {
            moneySlots.get(i).add(cashMachineToRemove.getMoneySlots().get(i).getQuantity());
        }
    }

    /*
    * Method to calculate and remove money from the slot based on the amount that the customer will take
    * */
    private int removeFromSlot(MoneySlot moneySlot, int totalToRemove, MoneySlot moneyRemovedSlot) {

        if (totalToRemove == 0) return totalToRemove;
        //If there is not enough bills or the amount is not enough will continue with the others slots of the machine
        if ((totalToRemove - moneySlot.getAmount()) < 0 || (moneySlot.getTotalInSLot() - moneySlot.getAmount()) < 0)
            return totalToRemove;
        totalToRemove = totalToRemove - moneySlot.getAmount();
        moneySlot.remove(1);
        moneyRemovedSlot.add(1);

        totalToRemove = removeFromSlot(moneySlot, totalToRemove, moneyRemovedSlot);

        return totalToRemove;
    }
}
