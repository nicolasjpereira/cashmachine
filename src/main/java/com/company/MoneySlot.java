package com.company;

/**
 * Created by Nicolas Pereira on 6/10/16.
 *
 *
 * The cash machine has MoneySlots for each amount.
 */
public class MoneySlot {
    private int amount;
    private int quantity = 0;

    public MoneySlot(int amount) {
        this.amount = amount;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public int getTotalInSLot() {
        return amount * quantity;
    }

    @Override
    public String toString() {
        return "MoneySlot{" +
                "amount=" + amount +
                ", quantity=" + quantity +
                '}';
    }

    public int add(int quantity) {
        this.quantity = this.quantity + quantity;
        return getTotalInSLot();
    }

    /*
    * The method will remove the quantity need.
    *
    * @param quantity Quantity of bills that will be removed
    * */
    public void remove(int quantity) {
        this.quantity = this.quantity - quantity;
        if (this.quantity < 0 ) this.quantity = 0;
    }

    public boolean isValidRemove(int quantity) {
        return this.quantity >= quantity;
    }
}
