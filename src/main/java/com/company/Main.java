package com.company;

import com.company.exceptions.InvalidInputException;

import java.io.BufferedReader;
import java.io.InputStreamReader;

/**
 * Created by Nicolas Pereira on 6/10/16.
 */

public class Main {
    public static void main(String[] args) {

        AppManager appManager = new AppManager();
        CashMachine cashMachine = new CashMachine();

        boolean running = true;
        try {
            do {
                System.out.println("Ready...");
                BufferedReader bufferRead = new BufferedReader(new InputStreamReader(System.in));
                String inputString = bufferRead.readLine();
                String[] inputStringArray = inputString.split(" ");
                String actionCommand = inputStringArray[0].toLowerCase();

                try {
                    if (inputString.contains("-")) throw new InvalidInputException();
                    switch (actionCommand) {
                        case AppManager.SHOW:
                            appManager.show(cashMachine);
                            break;
                        case AppManager.PUT:
                            appManager.put(inputStringArray, cashMachine);
                            break;
                        case AppManager.TAKE:
                            appManager.take(inputStringArray, cashMachine);
                            break;
                        case AppManager.CHANGE:
                            appManager.change(inputStringArray, cashMachine);
                            break;
                        case AppManager.QUIT:
                            running = false;
                            System.out.println("Bye");
                            break;
                        default:
                            System.out.println("Wrong Command.");
                    }
                } catch (Exception e) {
                    System.out.println(e.getMessage());
                }
            } while (running);
        } catch (Exception e) {
            System.out.println("Sorry the application stops. Please Restart application.");
        }
    }
}
