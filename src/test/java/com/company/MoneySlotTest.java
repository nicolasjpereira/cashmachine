package com.company;

import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class MoneySlotTest {

    @Test
    public void whenMoreMoneyIsAddedToSlotShouldUpdateTotalMoneyInSlot() {
        MoneySlot moneySlot = new MoneySlot(20);

        assertEquals(100,moneySlot.add(5));
    }

    @Test
    public void whenMoneyIsRemovedFromSlotShouldUpdateTotalMoneyInSlot() {
        MoneySlot moneySlot = new MoneySlot(20);
        moneySlot.add(5);
        moneySlot.remove(1);

        assertEquals(4, moneySlot.getQuantity());
    }

    @Test
    public void whenMoreMoneyThanTheActualAmountInSlotIsRemovedShouldReturnTheAmountOfMoneyRemoved() {
        MoneySlot moneySlot = new MoneySlot(20);
        moneySlot.add(3);
        moneySlot.remove(4);
        assertEquals(0, moneySlot.getQuantity());
    }

    @Test
    public void ifTheAmoutToRemoveIsNotValidShouldReturnFalse() {
        MoneySlot moneySlot = new MoneySlot(20);
        moneySlot.add(3);

        assertFalse(moneySlot.isValidRemove(5));
    }

    @Test
    public void ifTheAmoutToRemoveIsValidShouldReturnFalse() {
        MoneySlot moneySlot = new MoneySlot(20);
        moneySlot.add(3);

        assertTrue(moneySlot.isValidRemove(2));
    }
}