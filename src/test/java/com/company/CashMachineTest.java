package com.company;

import com.company.exceptions.InvalidAmountException;
import com.company.exceptions.InvalidArgumentException;
import com.company.exceptions.NoChangeException;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class CashMachineTest {

    private CashMachine cashMachine;

    @Before
    public void setUp() {
        cashMachine = new CashMachine();
    }

    @Test
    public void forEmptyMachineTotalShouldBeZero() {
        assertEquals(0, cashMachine.getTotal());
    }

    @Test
    public void forEmptyMachineShouldShowZero() {
        assertEquals("$0 0 0 0 0 0", cashMachine.show());
    }

    @Test
    public void addingTwoOfEachBillShouldShowTotalAndAmountOfBills() throws InvalidArgumentException {
        assertEquals("$76 2 2 2 2 2", cashMachine.put(new int[]{2, 2, 2, 2, 2}));
    }

    @Test
    public void addingOneOfEachBillShouldShowTotalAndAmountOfBills() throws InvalidArgumentException {
        assertEquals("$38 1 1 1 1 1", cashMachine.put(new int[]{1, 1, 1, 1, 1}));
    }

    @Test
    public void addingMixOfEachBillShouldShowTotalAndAmountOfBills() throws InvalidArgumentException {
        assertEquals("$123 5 2 0 0 3", cashMachine.put(new int[] {5, 2, 0, 0, 3}));
    }

    @Test(expected = InvalidArgumentException.class)
    public void addingInvalidArgumentsShouldDisplayAnError() throws InvalidArgumentException {
        cashMachine.put(new int[] {5, 2, 0, 0});
    }

    @Test(expected = InvalidArgumentException.class)
    public void takeWithInvalidArgumentsShouldDisplayAnError() throws InvalidAmountException, InvalidArgumentException {
        cashMachine.take(new int[]{5, 2, 0, 0});
    }

    @Test(expected = InvalidAmountException.class)
    public void takeWithInvalidAmountsShouldDisplayAnError() throws InvalidAmountException, InvalidArgumentException {
        cashMachine.take(new int[] {1, 1, 1, 1, 1});
    }

    @Test
    public void forInValidAmountsShouldReturnTrue() {
        assertFalse(cashMachine.validateAmounts(new int[]{1, 1, 1, 1, 1}));
    }

    @Test
    public void forValidAmountsShouldReturnTrue() {
        assertTrue(cashMachine.validateAmounts(new int[]{0, 0, 0, 0, 0}));
    }

    @Test
    public void forValidAmountShouldTakeTheAmountFromCashMachine() throws InvalidArgumentException, InvalidAmountException {
        cashMachine.put(new int[] {1, 1, 1, 1, 1});
        assertEquals("$0 0 0 0 0 0", cashMachine.take(new int[]{1, 1, 1, 1, 1}));
    }

    @Test
    public void forValidAmountShouldTakeTheAmountFromCashMachine2() throws InvalidArgumentException, InvalidAmountException {
        cashMachine.put(new int[] {2, 2, 2, 2, 2});
        assertEquals("$38 1 1 1 1 1", cashMachine.take(new int[] {1, 1, 1, 1, 1}));
    }

    @Test(expected = InvalidAmountException.class)
    public void takeMoreMoneyThanMoneyAvailableInCashMachineShouldDisplayAnError() throws NoChangeException, InvalidAmountException {
        cashMachine.change(100);
    }

    @Test
    public void takeMoneyFromCashMachineShouldReturnChangeAndRemoveFromMachine() throws InvalidArgumentException, NoChangeException, InvalidAmountException {
        cashMachine.put(new int[] {0, 2, 0, 0, 0});
        assertEquals("$10 0 1 0 0 0", cashMachine.change(10));
    }

    @Test
    public void takeMoneyFromCashMachineShouldReturnChangeAndRemoveFromMachine1() throws InvalidArgumentException, NoChangeException, InvalidAmountException {
        cashMachine.put(new int[] {5, 5, 5, 5, 5});
        assertEquals("$57 2 1 1 1 0", cashMachine.change(57));
    }

    @Test
    public void takeMoneyFromCashMachineShouldReturnChangeAndRemoveFromMachine2() throws InvalidArgumentException, NoChangeException, InvalidAmountException {
        cashMachine.put(new int[] {0, 0, 0, 0, 50});
        assertEquals("$49 0 0 0 0 49", cashMachine.change(49));
    }

    @Test
    public void takeMoneyFromCashMachineShouldReturnChangeAndRemoveFromMachine3() throws InvalidArgumentException, NoChangeException, InvalidAmountException {
        cashMachine.put(new int[] {0, 0, 0, 1, 50});
        assertEquals("$50 0 0 0 1 48", cashMachine.change(50));
    }

    @Test
    public void takeMoneyFromCashMachineShouldReturnChangeAndRemoveFromMachine4() throws InvalidArgumentException, NoChangeException, InvalidAmountException {
        cashMachine.put(new int[] {0, 1, 0, 1, 50});
        assertEquals("$50 0 1 0 1 38", cashMachine.change(50));
    }
}